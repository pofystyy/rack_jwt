require_relative "token"

class MyApp

  include JWToken

  def call(env)
    @str = env["QUERY_STRING"]
    status = 200
    @header = {"Content-Type" => "text/html"
            }
    body = []
    [status, @header, body << verify ]
  end

  def verify
    payload["name"] == "Tanya"  ? user_token : "no"
  end

  private

  def data
    Rack::Utils.parse_nested_query(@str)
  end

  def payload
    data.delete_if {| key, value | key == "pass" }
  end

  def user_token
    create_token(payload)
  end



end

